﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AzureTriying.Entity
{
    public class User
    {
        public int UserId { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public string City { get; set; }
        public int RoleId { get; set; }
        public Role Role { get; set; }
    }
}
