﻿using AzureTriying.Entity;
using System;
using System.Collections.Generic;
using System.Text;

namespace AzureTrying.Data.Abstract
{
    public interface IUserRepository: IGenericRepository<User>
    {
    }
}
