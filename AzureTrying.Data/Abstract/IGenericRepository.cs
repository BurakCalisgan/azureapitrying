﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AzureTrying.Data.Abstract
{
    public interface IGenericRepository<T> where T : class
    {
        //IQueryable<T> Find (Expression<Func<T, bool>> predicate);
        T GetById(int Id);
        List<T> GetAll();
        List<T> GetAll(List<string> includeList);
        void Create(T entity);
        void Edit(T entity);
        void Delete(T entity);
        void Save();
    }
}
