﻿using AzureTriying.Entity;
using Microsoft.AspNetCore.Builder;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AzureTrying.Data.Concrete.EfCore
{
    public static class SeedData
    {
        public static void Seed(IApplicationBuilder app)
        {
            AzureTryingDbContext context = app.ApplicationServices.GetRequiredService<AzureTryingDbContext>();
            context.Database.Migrate();

            if (!context.Roles.Any())
            {
                context.Roles.AddRange(
                    new Role() { RoleName = "Admin" },
                    new Role() { RoleName = "User" },
                    new Role() { RoleName = "Editor" }
                    );

                context.SaveChanges();
            }
            if (!context.Users.Any())
            {
                context.Users.AddRange(
                    new User() {Name="Pınar", Surname="Kayhan", City="Sakarya", RoleId=1 },
                    new User() {Name="Murat", Surname="Özdemir", City="Sakarya", RoleId=3 },
                    new User() {Name="Burak", Surname="Çalışgan", City="Çorum", RoleId=2 }
                    );

                context.SaveChanges();
            }
        }
    }
}
