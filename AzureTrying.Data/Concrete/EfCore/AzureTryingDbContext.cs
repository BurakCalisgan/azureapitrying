﻿using AzureTriying.Entity;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace AzureTrying.Data.Concrete.EfCore
{
    public class AzureTryingDbContext:DbContext
    {
        public AzureTryingDbContext(DbContextOptions<AzureTryingDbContext> options):base(options)
        {

        }

        public DbSet<User> Users { get; set; }
        public DbSet<Role> Roles { get; set; }

    }
}
