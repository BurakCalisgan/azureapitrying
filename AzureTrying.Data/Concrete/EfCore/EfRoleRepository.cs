﻿using AzureTriying.Entity;
using AzureTrying.Data.Abstract;
using System;
using System.Collections.Generic;
using System.Text;

namespace AzureTrying.Data.Concrete.EfCore
{
    public class EfRoleRepository:EfGenericRepository<Role>,IRoleRepository
    {
        public EfRoleRepository(AzureTryingDbContext context) : base(context)
        {

        }

        public AzureTryingDbContext AzureTryingDbContext
        {
            get { return _context as AzureTryingDbContext; }
        }
    }
}
