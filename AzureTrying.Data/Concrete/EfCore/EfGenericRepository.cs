﻿using AzureTrying.Data.Abstract;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AzureTrying.Data.Concrete.EfCore
{
    public class EfGenericRepository<T> : IGenericRepository<T> where T : class
    {
        protected readonly AzureTryingDbContext _context;
        public DbSet<T> DbSet { get; set; }
        public EfGenericRepository(AzureTryingDbContext context)
        {
            _context = context;
            DbSet = _context.Set<T>();
        }
        public void Create(T entity)
        {
            DbSet.Add(entity);
            Save();
            
        }

        public void Delete(T entity)
        {
            DbSet.Remove(entity);
            Save();
        }

        public void Edit(T entity)
        {
            _context.Entry(entity).State = EntityState.Modified;
            Save();
        }

        public List<T> GetAll()
        {
            return DbSet.ToList();
        }

        public List<T> GetAll(List<string> includeList)
        {
            throw new NotImplementedException();
        }

        public T GetById(int Id)
        {
            return DbSet.Find(Id);
        }

        public void Save()
        {
            _context.SaveChanges();
        }
    }
}
