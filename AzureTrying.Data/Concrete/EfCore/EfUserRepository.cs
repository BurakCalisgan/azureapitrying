﻿using AzureTriying.Entity;
using AzureTrying.Data.Abstract;
using System;
using System.Collections.Generic;
using System.Text;

namespace AzureTrying.Data.Concrete.EfCore
{
    public class EfUserRepository:EfGenericRepository<User>,IUserRepository
    {
        public EfUserRepository(AzureTryingDbContext context) : base(context)
        {

        }

        public AzureTryingDbContext AzureTryingDbContext
        {
            get { return _context as AzureTryingDbContext; }
        }
    }
}
