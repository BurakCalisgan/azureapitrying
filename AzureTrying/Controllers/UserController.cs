﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AzureTriying.Entity;
using AzureTrying.Data.Abstract;
using Microsoft.AspNetCore.Mvc;

namespace AzureTrying.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : Controller
    {
        private readonly IUserRepository _userRepository;
        public UserController(IUserRepository userRepository)
        {
            _userRepository = userRepository;
        }

        [HttpGet]
        public ActionResult<List<User>> Index()
        {
            return _userRepository.GetAll();
        }
        
        [HttpGet("{id}")]
        public ActionResult<User> Get(int id)
        {
            return _userRepository.GetById(id);
        }

        // POST api/User
        [HttpPost]
        public void Post([FromBody] User user)
        {
            _userRepository.Create(user);
        }

        // PUT api/User
        [HttpPut]
        public void Put(User user)
        {
            _userRepository.Edit(user);
        }

        // DELETE api/User
        [HttpDelete]
        public void Delete(User user)
        {
            _userRepository.Delete(user);
        }



    }
}