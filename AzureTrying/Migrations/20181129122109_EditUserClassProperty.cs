﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace AzureTrying.Migrations
{
    public partial class EditUserClassProperty : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "RoleName",
                table: "Roles",
                nullable: true,
                oldClrType: typeof(int));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<int>(
                name: "RoleName",
                table: "Roles",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);
        }
    }
}
